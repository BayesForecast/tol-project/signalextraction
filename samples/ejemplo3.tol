//////////////////////////////////////////////////////////////////////////////
// Ejemplo III
//////////////////////////////////////////////////////////////////////////////
#Require MMS;

Real PutRandomSeed(23);
  
Date begin = y2003m01;
Date end = y2009m12;
  
// boxcox  
//MMS::@Transformation boxcox = MMS::@Transformation.BoxCox::Default(0,0); 

// Especificamos Arima
Set arima = GetArimaFromLabel("P1_12DIF1_1AR0_0MA1_12"); 
  
Real P = Degree(ARIMAGetAR(arima)); 
Real D = Degree(ARIMAGetDIF(arima)); 
Real Q = Degree(ARIMAGetMA(arima));
  
Real ini = 100*Max(P+D, Q);
Date begin_ = Succ(begin, Mensual, -ini);
Serie r0 = SubSer(Gaussian(0, 0.2, Mensual), begin_, end);
  
Serie n0 = DifEq(ARIMAGetMA(arima)/ARIMAGetARI(arima), r0);
Serie residuals = SubSer(r0, begin, end);    
Serie noise = SubSer(n0, begin, end);


// Creamos el dataSet y las variables
MMS::@DataSet DS.PjA = MMS::Container::ReplaceDataSet([[
  Text _.name = "Ejemplo_A";
  Text _.description = "Data Set para el Ejemplo_A"
]]);

Anything DS.PjA::ReplaceVariable([[
  Text _.name = "VariableA1";
  Text _.source = "Ejemplo_A";
  Text _.expression = "Serie noise"
]]);

// Creamos el modelo y el submodelo
MMS::@Model model = MMS::Container::ReplaceModel([[
  Text _.name = "ModelA";
  Text _.version = "1";
  Set _.dataSets = [[ "Ejemplo_A" ]]
]]);
  
MMS::@Submodel submodel = model::CreateSubmodel([[
  Text _.name = "OutputA";
  NameBlock _.output = [[
    Text _.name = "VariableA1";
    Text _.variable = "VariableA1"
  ]];
  NameBlock _.noise = [[
    Text _.type = "ARIMA";
    Text _.arimaLabel = "P1_12DIF0_1AR1_0MA0_12"; 
    Real _.sigma = 1
  ]]
]]); 

// Creamos la estimaci�n MLE
Real estimNum = MMS::Container::FindEstimation([["ModelA","1","Estimate"]]);
Real If(estimNum, MMS::Container::RemoveEstimation(estimNum));

MMS::@Estimation estimation = MMS::Container::ReplaceEstimation([[
  Text _.name = "Estimate";
  MMS::@Model _.model = MMS::Container::GetModel([["ModelA","1"]]);
  MMS::@SettingsMultiMLE _.settings = [[
    Real _.showTraces =  True
  ]]
]]);

// Ejecuci�n de la estimaci�n
Real estimation::Execute(?);

// Extraccion de se�ales

Serie noise_ = estimation::GetModel.Results(?)::GetSubmodel(1)::GetNoise(?);
Set arima_ = estimation::GetModel.Results(?)::GetSubmodel(1)::GetARIMA(?);

//////////////////////////////////////////////////////////////////////////////
// Periodo           AR                 MA               DIF
//	1.0	1-0.960065460355237*B	 1	          1
//	12.0	     1      1+0.0872760382485312*B^12	1-B^12

Polyn tetha = arima_[2]["ma"];
Polyn ar1 = arima_[1]["ar"];
Polyn dif12 = arima_[2]["dif"];

Polyn phi1 = ar1*(1-B);
Polyn phi2 = Quotient(dif12/(1-B));

Real n = CountS(noise_);
Serie noiseB = SignalExtraction::ForecastingNoiseB(noise_, arima_, n/2);
Serie noiseF = SignalExtraction::ForecastingNoiseF(noise_, arima_, n/2);

Real v = VarS(SignalExtraction::ARIMAAlmagroEval_Residuals(arima_, noise_));

Set se_B = SignalExtraction::
  MSXBurman(tetha, SetOfPolyn(phi1, phi2), noise_, noiseB, noiseF);

Set se_New = SignalExtraction::
  signals(tetha, SetOfPolyn(phi1, phi2), noise_, noiseB, noiseF, v);
