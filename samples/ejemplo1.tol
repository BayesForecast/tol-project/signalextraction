//////////////////////////////////////////////////////////////////////////////
// Ejemplo I: Matriculaci�n de Veh�culos
//////////////////////////////////////////////////////////////////////////////
#Require SignalExtraction;

// Noise y ARIMA
Set ejemplo1 = Ois.Load("matriculacion.oza");  
Serie noise = ejemplo1[1][1];
Set arima = ejemplo1[1][2];

//////////////////////////////////////////////////////////////////////////////
// Periodo          AR                  MA                  DIF
//   1.0    1-0.764387741304042*B        1	             1
//  12.0     	1	   1-0.70335689859972*B^12          1-B^12
//////////////////////////////////////////////////////////////////////////////

Real n = CountS(noise);
Serie noiseF = SignalExtraction::ForecastingNoiseF(noise, arima, n/2);
Serie noiseB = SignalExtraction::ForecastingNoiseB(noise, arima, n/2);

// Separamos la diferencia
Polyn tetha = arima[2]["ma"];
Polyn dif = arima[2]["dif"];
Polyn phi = arima[1]["ar"];
Polyn phi1 = phi*(1-B); // Polinomio de grado 2
Polyn phi2 = Quotient(dif/(1-B)); // 1+B+B^2+...+B^11

Real v = VarS(SignalExtraction::ARIMAAlmagroEval_Residuals(arima, noise));


// 1� Utilizando la funci�n MSXBurman
Set CSE = SignalExtraction::MSXBurman(tetha, SetOfPolyn(phi1, phi2),
          noise, noiseB, noiseF);


// 2� Utilizando la funci�n signals
Set CSE_New = SignalExtraction::signals(tetha, SetOfPolyn(phi1, phi2), noise, noiseB, noiseF, v);

// Grado del numerador: 12
// Grado del denominador: 13
// La parte residual parece un MA negativo

//////////////////////////////////////////////////////////////////////////////
// Serie u1=CSE_New[1];
// Serie u2=CSE_New[2];
// Serie u3=CSE_New[3];
// Real m3=AvrS(u3);
// Serie u4=CSE_New[4];
// Real m4=AvrS(u4);
// Serie u2 := u2+m3+m4;
// Serie u3:= u3-m3;
// Serie u4:= u4-m4;
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
// Ejemplo II: Modelo de Empresas
//////////////////////////////////////////////////////////////////////////////

// Noise y ARIMA
Set ejemplo2 = Ois.Load("empresas.oza");  
Serie noise2 = ejemplo2[1][1];
Set arima2 = ejemplo2[1][2];

// Serie residuos = SignalExtraction::
//   ARIMAAlmagroEval_Residuals(arima2, noise2);


//////////////////////////////////////////////////////////////////////////////
// Periodo          AR                         MA                  DIF
//   1.0    1-0.121*B-0.139*B^2-0.121*B^3       1	           1-B
//  12.0    1-0.228420455719273*B^12	        1                    1
//////////////////////////////////////////////////////////////////////////////

Real n2 = CountS(noise2);
Serie noiseF2 = SignalExtraction::ForecastingNoiseF(noise2, arima2, n2/2);
Serie noiseB2 = SignalExtraction::ForecastingNoiseB(noise2, arima2, n2/2);

// Polinomios
Polyn tetha2 = arima2[2]["ma"];
Polyn dif2 = arima2[1]["dif"];
Polyn ar2 = arima2[1]["ar"];
Polyn ar12 = arima2[2]["ar"];
Set raices_ar2 = GetPolynRoots(ar2, 0.001);
// Text SignalExtraction::Espectro_Ratio((1)/ar2, 1);
// Raiz real y un par de complejas conjugadas

Set fac2 = SignalExtraction::STFactorization(ar12);
Polyn phi1_ = dif2*fac2[1];
Polyn phi2_ = ar2;
Polyn phi3_ = fac2[2];

Real v2 = VarS(SignalExtraction::ARIMAAlmagroEval_Residuals(arima2, noise2));


// 1� Utilizando la funci�n MSXBurman
Set CSE2_Burman = SignalExtraction::
  MSXBurman(tetha2, SetOfPolyn(phi1_, phi2_, phi3_), noise2, noiseB2, noiseF2);

Set CSE_Burman = {[[
    Serie Burman.Noise         = CSE2_Burman[1];
    Serie Burman.Residuals     = CSE2_Burman[2];
    Serie Burman.SeasonalCycle = CSE2_Burman[3];
    Serie Burman.RegularTrend  = CSE2_Burman[4];
    Serie Burman.RegularNoise  = CSE2_Burman[5]
]]};


// 2� Utilizando la funci�n se_Clau
Set CSE2_Signals = SignalExtraction::
signals(tetha2, SetOfPolyn(phi1_, phi2_, phi3_), noise2, noiseB2, noiseF2, v2);


Set CSE_Signals = {[[
    Serie Signals.Noise         = CSE2_Signals[1];
    Serie Signals.Residuals     = CSE2_Signals[5];
    Serie Signals.SeasonalCycle = CSE2_Signals[2];
    Serie Signals.RegularTrend  = CSE2_Signals[3];
    Serie Signals.RegularNoise  = CSE2_Signals[4]
]]};


Set compare = Traspose([[CSE_Burman,CSE_Signals]]);


