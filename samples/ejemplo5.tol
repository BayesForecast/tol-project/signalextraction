
//////////////////////////////////////////////////////////////////////////////
// EjemploA
/////////////////////////////////////////////////////////////////////////////

#Require MMS;

Set ejemploA = Ois.Load("C:/EjemploA.oza");

Serie noise = ejemploA[1][1];
Set arima = ejemploA[1][2];

//////////////////////////////////////////////////////////////////////////////
// Periodo       AR              MA                    DIF
// 1.0	          1          1-0.0828*B                1-B
// 12.0	     1-0.798*B^12        1	                 1


Polyn tetha = SetProd(EvalSet(arima, Polyn (Set se) {se["ma"]}));
Polyn fi1 = arima[1]["dif"];
Set d = SignalExtraction::STFactorization(arima[2]["ar"]);
Polyn fi2 = d[1];
Polyn fi3 = d[2];

// Grado del numerador: 1;
// Grado del denominador: 13
// Residuos fuera de las bandas el 1, 2 y 12, negativos

Real n = CountS(noise);
Serie noiseB = SignalExtraction::ForecastingNoiseB(noise, arima, n/2);
Serie noiseF = SignalExtraction::ForecastingNoiseF(noise, arima, n/2);

Set se_B = SignalExtraction::
  MSXBurman(tetha, SetOfPolyn(fi1*fi2, fi3), noise, noiseB, noiseF);

Real v = VarS(SignalExtraction::ARIMAAlmagroEval_Residuals(arima, noise));
Set se_New = SignalExtraction::
  signals(tetha, SetOfPolyn(fi1*fi2, fi3), noise, noiseB, noiseF, v);


//////////////////////////////////////////////////////////////////////////////

// OBSERVACIÓN

Serie u1 = se_New[1];
Serie u2 = se_New[2];
Serie u3 = se_New[3];
Real m3 = AvrS(u3);
Serie u4 = se_New[4];
Real m4 = AvrS(u4);

Serie u2 := u2+m3+m4;
Serie u3 := u3-m3;
Serie u4 := u4-m4;

// La parte irregular sale rara: u4


// Creamos el dataSet y las variables
MMS::@DataSet DS.PjA = MMS::Container::ReplaceDataSet([[
  Text _.name = "EjemploA";
  Text _.description = "Data Set para el EjemploA"
]]);

Anything DS.PjA::ReplaceVariable([[
  Text _.name = "VariableA";
  Text _.source = "EjemploA";
  Text _.expression = "Serie noise"
]]);

// Creamos el modelo y el submodelo
MMS::@Model model = MMS::Container::ReplaceModel([[
  Text _.name = "ModelA";
  Text _.version = "1";
  Set _.dataSets = [[ "EjemploA" ]]
]]);
  
MMS::@Submodel submodel = model::CreateSubmodel([[
  Text _.name = "OutputA";
  NameBlock _.output = [[
    Text _.name = "VariableA";
    Text _.variable = "VariableA"
  ]];
  NameBlock _.noise = [[
    Text _.type = "ARIMA";
    Text _.arimaLabel = "P1_12DIF1_0AR0_12MA0_0"; 
    Real _.sigma = 1
  ]]
]]); 

// Creamos la estimación MLE
MMS::@Estimation estimation = MMS::Container::ReplaceEstimation([[
  Text _.name = "Estimate";
  MMS::@Model _.model = MMS::Container::GetModel([["ModelA","1"]]);
  MMS::@SettingsMultiMLE _.settings = [[
    Real _.showTraces =  True
  ]]
]]);

// Ejecución de la estimación
Real estimation::Execute(?);

// ARIMA y noise

Set arima_ = estimation::GetModel.Results(?)::GetSubmodel(1)::GetARIMA(?);
Serie noise_ = estimation::GetModel.Results(?)::GetSubmodel(1)::GetNoise(?);

//////////////////////////////////////////////////////////////////////////////
// Periodo           AR              MA              DIF
//   1.0              1               1	             1-B
//  12.0	  1-0.925B^12         1	               1

Real m = CountS(noise_);
Serie noiseB_ = SignalExtraction::ForecastingNoiseB(noise_, arima_, m/2);
Serie noiseF_ = SignalExtraction::ForecastingNoiseF(noise_, arima_, m/2);

 

Polyn tetha_ = SetProd(EvalSet(arima_, Polyn (Set se) {se["ma"]}));
Polyn fi1_ = arima_[1]["dif"];
Set d_ = SignalExtraction::STFactorization(arima_[2]["ar"]);
Polyn fi2_ = d_[1];
Polyn fi3_ = d_[2];

Set SE1_B = SignalExtraction::
  MSXBurman(tetha_, SetOfPolyn(fi1_*fi2_, fi3_), noise_, noiseB_, noiseF_);

Real v_ = VarS(SignalExtraction::ARIMAAlmagroEval_Residuals(arima_, noise_));
Set SE1_New = SignalExtraction::
  signals(tetha_, SetOfPolyn(fi1_*fi2_, fi3_), noise_, noiseB_, noiseF_, v_);



