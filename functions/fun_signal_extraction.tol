//////////////////////////////////////////////////////////////////////////////
// FILE   : fun_signal_extraction.tol
// PURPOSE: Signal extraction function
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
Polyn TunnicliffeWilsonAlg2(Polyn cBF, Polyn tetha)
//////////////////////////////////////////////////////////////////////////////
{
  Polyn c       = SetSum(For(0, Degree(cBF), Polyn (Real j) {
    Coef(cBF, j)*B^j
  }));
  Real degTetha = Degree(tetha);
  Real degC     = Degree(c); 
  Polyn tethaF  = ChangeBF((F^degC)*tetha);

  Matrix A1     = PolMat(tetha, degC+1, degC+1);
  Matrix invInd = SetRow(Range(degC+1, 1,-1));
  Matrix A2     = PivotByColumns(PolMat(tethaF, degC+1, degC+1),invInd);

  Matrix A = A1+A2;
  Matrix cCoef = Reverse(PolMat(c, degC+1, 1));
  Matrix G = MultiFitLinear(A,cCoef);
  MatPol(Reverse(Tra(G)))
};

//////////////////////////////////////////////////////////////////////////////
Serie GetWKFilter
(
  Polyn cBF,
  Polyn tetha, 
  Serie noise,
  Serie noiseB,
  Serie noiseF
)
//////////////////////////////////////////////////////////////////////////////
{
  Polyn g   = TunnicliffeWilsonAlg2(cBF, tetha);
  //Polyn check = g*ChangeBF(tetha)+ChangeBF(g)*tetha;
  //Polyn difcBF = cBF-check;
  //Matrix checkdifcBFMat = PolMat(difcBF, Degree(difcBF)+1, 1);
  Real m = CountS(noiseB);
  Polyn expGtetha = Expand(g/tetha, m/2);
  //Polyn checkexpGtetha = expGtetha*tetha;
  Serie filterB = expGtetha:(noiseB<<noise<<noiseF);
  Serie filterF = ChangeBF(expGtetha):(noiseB<<noise<<noiseF);
  Serie wkFilter = (filterB+filterF);
  SubSer(wkFilter, First(noise), Last(noise))
};

//////////////////////////////////////////////////////////////////////////////
Set GetMinAutoVarGenFun(Polyn numPol, Polyn denPol)
//////////////////////////////////////////////////////////////////////////////
{
  Real step = 0.0001; 
  Set info = DrawRatio(numPol/denPol, Range(-1, 1-step, step));
  Real min = SetMin(Traspose(info)[2]);
  SetOfAnything(min, info) 
};

//////////////////////////////////////////////////////////////////////////////
// MSX : Minimum Seasonal Extraction 
// (pag.325 del pdf Seasonal Adjustment by Signal Extraction (Burman)
Set MSXBurman
(
  Polyn tetha, 
  Set phiRootSet,
  Serie noise,
  Serie noiseB,
  Serie noiseF
)
//////////////////////////////////////////////////////////////////////////////
{
  Polyn u  = tetha*ChangeBF(tetha);
  Polyn ux = ChangeSymBFCosx(u); 
  Set polSet = EvalSet(phiRootSet, Set(Polyn phiRoot) {
    Polyn vRoot  = phiRoot*ChangeBF(phiRoot);
    Polyn vRootx = ChangeSymBFCosx(vRoot);
    SetOfPolyn(vRoot, vRootx) 
  });
  Set traPolSet = Traspose(polSet);
  Polyn phix    = SetProd(traPolSet[2]);
  Set divIniSet = PolDivision(ux, phix);
  Polyn qinix = divIniSet[1]; 
  Polyn rx    = divIniSet[2];

  Set factorSet = InvPolExtGCD(traPolSet[2]);

  If(EQ(Card(factorSet),0), {
    WriteLn("ERROR 1","E");
    WriteLn("Falla en la funci�n: InvPolExtGCD","E");
    Empty
  }, {
    Set rxIniSet = EvalSet(factorSet, Polyn(Polyn fx){ rx*fx });
    Set divSet = For(1, Card(factorSet), Set(Real k){
      Polyn rxIni = rxIniSet[k];
      Polyn vRootx = polSet[k][2];
      PolDivision(rxIni, vRootx)
    });
// Hay que meter otro mecanismo de chequeo, si no se puede hacer alguna
// divisi�n del for anterior, un subconjunto de divSet queda vacio y no se 
// puede transponer. Por lo tanto genera infinitos errores en adelante
    Set traDivSet = Traspose(divSet);
    Set rxSet = traDivSet[2];
    Set qxSet = traDivSet[1];
    Polyn qx = qinix+SetSum(qxSet);

    Set filterSet = For(1, Card(phiRootSet), Set(Real k){
      [[
        Set minSet = GetMinAutoVarGenFun(rxSet[k], polSet[k][2]);
        Real mingx = minSet[1];
        Polyn tethaRootx = rxSet[k]-polSet[k][2]*mingx;
        Polyn tethaRoot = ChangeCosxSymBF(tethaRootx);
        Polyn phiEst    = ChangeCosxSymBF(Quotient(phix/polSet[k][2]));
        Serie rootFilter = 
          GetWKFilter(tethaRoot*phiEst, tetha,  noise, noiseB, noiseF)
      ]] 
    }); 
    Real mingRea = SetSum(EvalSet(filterSet, Real(Set reg){ reg::mingx }));
    Set signalSet = EvalSet(filterSet, Serie(Set reg){ 
      reg::rootFilter
    });

    Ratio gR = ChangeCosxSymBF(qx+mingRea)/ChangeCosxSymBF(1);
    Serie res = noise-SetSum(signalSet);
//  Set modelDefRes = ModelDef(res, 1, 0, 1, 0, 1, SetOfPolyn(1), SetOfPolyn(1-0.1*B), Empty, Empty);
//  Set estModelRes = Estimate(modelDefRes);
//  Set forModelRes = CalcForecasting(estModelRes, Last(res), Last(res), 3, 0.005/2);

    Set series = SetOfSerie(noise, res)<<signalSet
  })
};

/*
//////////////////////////////////////////////////////////////////////////////
Set MSXFromModel(Set estModel)
//////////////////////////////////////////////////////////////////////////////
{
  Serie noise = estModel::Series::Noise;  
  Set calcFor = 
   CalcForecasting(estModel, Last(noise), Last(noise), 400, 0.05/2);
  Set calcForBack = 
   CalcForecastingF(estModel, First(noise), First(noise), 400, 0.05/2);

  Serie noiseF = calcFor::prevNoise;
  Serie noiseB = calcForBack::prevNoise;

  Polyn tetha = SetProd(estModel::Definition::MA);
  Polyn phiT = 1-B;
  Polyn phiS = SetProd(estModel::Definition::AR);

  Set CSE = MSXBurman(tetha, SetOfPolyn(phiT*phiS), noise, noiseB, noiseF)
};
*/

//////////////////////////////////////////////////////////////////////////////
Set MSXFromArima(Serie noise, Set arima)
//////////////////////////////////////////////////////////////////////////////
{
  Serie noiseB = ForecastingNoiseB(noise, arima, 400);
  Serie noiseF = ForecastingNoiseF(noise, arima, 400);
  Polyn tetha = SetProd(EvalSet(arima, Polyn (Set s) {s["ma"]}));
  Set conj = EvalSet(arima, Set (Set se) {STFactorization(se["ar"])})<<
             EvalSet(arima, Set (Set se) {STFactorization(se["dif"])});
  Set phis = EvalSet (Traspose(conj), Polyn (Set se) {SetProd(se)});   
  MSXBurman(tetha, phis, noise, noiseB, noiseF)
};


//////////////////////////////////////////////////////////////////////////////
// Seasonal Trend Factorization
// Falta considerar el caso (1-phi*B^s)^D
Set STFactorization ( Polyn pol )
//////////////////////////////////////////////////////////////////////////////
{
  Real deg = Degree(pol);
  Set coefSet = Unique(For(1, deg-1, Real (Real i) { Coef(pol, i) }));
  Set If (Card(coefSet)==1, {
    Case(deg == 0, SetOfPolyn (Polyn 1, Polyn 1),
         deg == 1, SetOfPolyn (pol, Polyn 1),
         coefSet[1]==0, {
           Real coef = If (Coef(pol, deg)<0,
                          (-Coef(pol, deg))^(1/deg),
                           Coef(pol, deg)^(1/deg)); // Corregir, si el coeficiente es mayor que 0
           Polyn tend = 1-coef*B;
           Set div = PolDivision(pol, tend);
           Polyn est = If (Coef(div[2],0)<1/999999,  div[1], ?);
           SetOfPolyn( tend, est )
         },
         1, SetOfPolyn( pol, Polyn 1)
    )},
    SetOfPolyn( pol, Polyn 1)
  )
};

//////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////


//////////////////////////////////////////////////////////////////////////////
Set GetMinimum(Polyn numBF, Polyn denBF, Real sigma)
//////////////////////////////////////////////////////////////////////////////
{
  Set domain = Range(0, pi, 0.001);
  Code Abs_ = FindCode("Real", "Abs");
  Set conjunto = EvalSet(domain, Set (Real w){
    Real y1 = SetSum( For (0, Degree(numBF), Real (Real i) {
      Coef(numBF, i)*Cos(i*w)*If(i==0, 1, 2)
    }));
    Real y2 = SetSum( For (0, Degree(denBF), Real (Real i) {
      Coef(denBF, i)*Cos(i*w)*If(i==0, 1, 2)
    }));
    Real y3= y1*sigma;
    // Variable
    If(Or(Abs_(y3/y2)> 100000, (y3/y2)<0 ), [[w, ? ]], [[ w, y3/y2 ]])
  });
  [[ Real SetMin(Traspose(conjunto)[2]), conjunto ]]
};

//////////////////////////////////////////////////////////////////////////////
Set signals
(
  Polyn tetha,
  Set phis,
  Serie noise,
  Serie noiseB,
  Serie noiseF,
  Real v
)
//////////////////////////////////////////////////////////////////////////////
{

  // Descomponemos la estructura arima
  Set div = SymPolDivision(tetha, SetProd(phis));
  Polyn q = div[1];
  Polyn r = div[2];

  // Separamos en fracciones simples
  Set phis_ = EvalSet(phis, Polyn (Polyn phi) {
    phi*ChangeBF(phi)
  });
  Set tetha_ = DecoSimpleFactor(r, phis_);

  Set minimos = For(1, Card(phis_), Set (Real i){
   [[
      Set minC = GetMinimum(tetha_[i], phis_[i], v),
      Real min = minC[1],
      Polyn tetha_n = tetha_[i]-phis_[i]*min
   ]]
  });
  Set minimosT = Traspose(minimos);

  Real minT =SetSum(minimosT[2]);
  Polyn qT = q + minT;
  Set Append(phis_, [[ Polyn 1 ]]);
  Set Append(minimosT[3], [[ qT ]]);

  Set series = For (1, Card(phis_), Serie (Real i) {
    Polyn cBF = SetProd(phis_ - [[ phis_[i] ]])*minimosT[3][i];
    Serie aux = GetWKFilter(cBF, tetha, noise, noiseB, noiseF);
    PutName("Serie "<<i, aux)
  });

  //Serie res = noise-SetSum(series);

  Set SetOfSerie(noise)<<series

};


//////////////////////////////////////////////////////////////////////////////
Text _.autodoc.member.SeasonalCycleTrendDecomp = 
"Estimates a given ARIMA model, eventually with inputs, and calls method "
"SignalExtraction::signals in order to get a seasonal cycle-trend "
"decomposition.\n"
"Arguments:\n"
"  Serie output : the time series to be decomposed (transformed if needed)\n"
"  Set arima    : Set of two @ARIMAStruc: first regular, second seasonal\n"
"  Set inputs   : Set (posibly empty) of inputs to filter the output\n"
"Returns a NameBlock with these members:\n"
"  Serie Output        : the same given time series; \n"
"  Serie Filter        : sum of estimated input effects\n"
"  Serie Noise         : output filtered of input effects\n"
"  Serie SeasonalCycle : seasonal cycle signal\n"
"  Serie RegularTrend  : regular trend signal\n"
"  Serie RegularNoise  : regular noise; \n"
"  Set Arima; \n"
"  Set ModelEst = modelEst \n"
"";
//////////////////////////////////////////////////////////////////////////////
NameBlock SeasonalCycleTrendDecomp(
  Serie output, 
  Set arima, 
  Set inputs)
//////////////////////////////////////////////////////////////////////////////
{ 
  Real  Period = arima[2]->Periodicity; 
  Polyn regDif = arima[1]->DIF;
  Polyn seaDif = arima[2]->DIF;
  Polyn regAR  = arima[1]->AR;
  Polyn regMA  = arima[2]->AR;
  Polyn seaAR  = arima[1]->MA;
  Polyn seaMA  = arima[2]->MA;
  
  Set modelDef = {@ModelDef( 
      Serie output; 
      Real FstTransfor = 1; 
      Real SndTransfor = 0; 
      Real Period; 
      Real Constant = 0; 
      Polyn Dif = seaDif*regDif; 
      Set AR = SetOfPolyn(regAR,regMA); 
      Set MA = SetOfPolyn(seaAR,seaMA); 
      Set inputs;
      Set NonLinInput = Copy(Empty) 
    )};
  
  Set modelEst = Estimate(modelDef);
  
  Set def = modelEst::Definition;
  Serie filtered = modelEst::Series::Noise;

  Polyn regAR := (def->AR)[1];
  Polyn regMA := (def->MA)[1];
  Polyn seaAR := (def->AR)[2];
  Polyn seaMA := (def->MA)[2];
  
  Set Arima = [[
    @ARIMAStruct(1,     regAR,regMA,regDif),
    @ARIMAStruct(Period,seaAR,seaMA,seaDif)
  ]];
  
  
  Polyn tetha = seaMA;
  
  Polyn phi1 = regAR*(1-B);
  Polyn phi2 = Quotient(seaDif/(1-B));
  
  Real n = CountS(output);
  
  Serie filteredB = SignalExtraction::ForecastingNoiseB(filtered, Arima, Round(n/2));
  Serie filteredF = SignalExtraction::ForecastingNoiseF(filtered, Arima, Round(n/2));
  Real v = VarS(SignalExtraction::ARIMAAlmagroEval_Residuals(Arima, filtered));
  
  Set signal = SignalExtraction::
    signals(tetha, SetOfPolyn(phi1, phi2), output, filteredB, filteredF, v);
    
  NameBlock results = 
  [[
    Serie Output = output;
    Serie Filter = modelEst::Series::Filter;
    Serie Noise = filtered;
    Serie SeasonalCycle = signal[3];
    Serie RegularTrend = signal[2];
    Serie RegularNoise = signal[4];
    Set Arima;
    Set ModelEst = modelEst
  ]]
};
 